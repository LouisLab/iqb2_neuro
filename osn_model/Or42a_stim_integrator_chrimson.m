function [times,y_ifb]=Or42a_stim_integrator_chrimson(x, dt)
% parameter optimization based on ephys data of Elena Knoche in collaboration with Sharpe
% lab (CRG)
% Ephys conducted on Or42a single functional OSN expressing ChrimsonR

warning off

delay=0.05; %in s
additional_relaxation_time=15;%default=15;
fps=round(1/dt);
binning_factor=2; %number of bins per frame rate
bin_size=round(fps/binning_factor); %given in frames

% PARAMETER SET obtained after optimization by Marco Musy (with Elena)
%% 161212, LIGHT stimulation
a1=0;
a2=0.15;
a3=0.0103327; % 0.00511586;
c1=11246.2; % 10266.3;
c2=0.664913; % 0.660383;
c3=425.62; % 424.179;
c4=10.2937; % 14.9543; 
c5=621.364; % 974.236;
hillK=2; %Marco's hillp
hillS=0.0301372; % 0.0516378; %Marco's hillk
hill_charTime=1000; % 1000 disables correction
c2_charTime=1000; % 1000 disables correction
scf_x_ifb = 1;

x=[x(1)*ones(additional_relaxation_time*fps,1); x]; % allow for relaxation phase to be added
times=linspace(0,length(x),length(x))*dt;
Tspan = [0 max(times)];
IC = [1 1]; % Initial conditions

[t_ifb y_ifb]=ode23tb(@(t,y) ifb_ode(t,y,times,x*scf_x_ifb,a1,a2,a3,c1,c2,c3,c4,c5,hillK,hillS,hill_charTime,c2_charTime),Tspan,IC); %note that 15s doesn't work for linear slopes. But 23s does. 

u_ifb=interp1(t_ifb,y_ifb(:,1),times)'; %interpolation of y vector between integration steps
y_ifb=interp1(t_ifb,y_ifb(:,2),times)'; %interpolation of y vector between integration steps
%
y_ifb=y_ifb((length(ones(additional_relaxation_time*fps,1))+1):end); %remove part corresponding to relaxation phase
u_ifb=u_ifb((length(ones(additional_relaxation_time*fps,1))+1):end); %remove part corresponding to relaxation phase
x=x((length(ones(additional_relaxation_time*fps,1))+1):end); %remove part corresponding to relaxation phase
times=times((length(ones(additional_relaxation_time*fps,1))+1):end); %remove part corresponding to relaxation phase
