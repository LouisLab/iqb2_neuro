function [times,y_ifb]=Or42a_ifb_integrator_chrimson(x,osn,dt)
% parameter optimization based on ephys data of Elena Knoche in collaboration with Sharpe
% lab (CRG)
% Ephys conducted on Or42a single functional OSN expressing ChrimsonR

warning off

delay=0.05; %in s
additional_relaxation_time=15;%default=15;
fps=round(1/dt);
binning_factor=2; %number of bins per frame rate
bin_size=round(fps/binning_factor); %given in frames

% PARAMETER SET obtained after optimization by Marco Musy (with Elena)
%% 161212, LIGHT stimulation
a1=0;
a2=0.15;
a3=0.0103327; % 0.00511586;
c1=11246.2; % 10266.3;
c2=0.664913; % 0.660383;
c3=425.62; % 424.179;
c4=10.2937; % 14.9543; 
c5=621.364; % 974.236;
hillK=2; %Marco's hillp
hillS=0.0301372; % 0.0516378; %Marco's hillk
hill_charTime=1000; % 1000 disables correction
c2_charTime=1000; % 1000 disables correction
scf_x_ifb = 1;

x=[x(1)*ones(additional_relaxation_time*fps,1); x]; % allow for relaxation phase to be added
times=linspace(0,length(x),length(x))*dt;
Tspan = [0 max(times)];
IC = [1 1]; % Initial conditions

[t_ifb y_ifb]=ode23tb(@(t,y) ifb_ode(t,y,times,x*scf_x_ifb,a1,a2,a3,c1,c2,c3,c4,c5,hillK,hillS,hill_charTime,c2_charTime),Tspan,IC); %note that 15s doesn't work for linear slopes. But 23s does. 

u_ifb=interp1(t_ifb,y_ifb(:,1),times)'; %interpolation of y vector between integration steps
y_ifb=interp1(t_ifb,y_ifb(:,2),times)'; %interpolation of y vector between integration steps
%
y_ifb=y_ifb((length(ones(additional_relaxation_time*fps,1))+1):end); %remove part corresponding to relaxation phase
u_ifb=u_ifb((length(ones(additional_relaxation_time*fps,1))+1):end); %remove part corresponding to relaxation phase
x=x((length(ones(additional_relaxation_time*fps,1))+1):end); %remove part corresponding to relaxation phase
times=times((length(ones(additional_relaxation_time*fps,1))+1):end); %remove part corresponding to relaxation phase

%% Goodness of fit
Pearson_coefficient=corrcoef(osn(:,1),y_ifb);
Pearson_coefficient=Pearson_coefficient(1,2);
cvRMSE=sqrt(mean((osn(:,1)-y_ifb).^2))/mean(osn(:,1));
%
performance.corrcoef=Pearson_coefficient;
performance.cvRMSE=cvRMSE;
%%

%% PLOTTING
clf
subplot(2,1,1),plot(times,x,'r','LineWidth',2)
ylabel('Stimulus intensity')
box off
%xlim([0 30])
ylim([0 20]) 

subplot(2,1,2),plot(times,osn,'k','LineWidth',2),hold on
plot(times,y_ifb,'b','LineWidth',2)
hold off
ylabel('Firing rate (Hz)')
xlabel('Time (s)')
%legend('Experimental activity','Predicted activity','Quasi steady state approximation')
box off
%xlim([0 30])
ylim([0 45])
str_title{1}=['Pearsson correlation coefficient -- Test model: ',num2str(Pearson_coefficient)];
str_title{2}=['Coefficient of variation of RMSE -- Test model: ',num2str(cvRMSE)];
title(str_title)
