function dy = ifb_ode(t,y,time_vector,input_signal,a1,a2,a3,c1,c2,c3,c4,c5,hillK,hillS,hill_charTime,c2_charTime) 
additional_relaxation_time=60;
% st = input signal at time t
% y(1) = u
% y(2) = y
% Note that t is given in seconds

st = interp1(time_vector,input_signal,t);
dy = zeros(2,1); 

t_real=(t-additional_relaxation_time);

if hill_charTime<1000
timeDependent_c5=c5*(1+(28./1216.)*((t_real^4)/(t_real^4+hill_charTime^4)));
else 
timeDependent_c5=c5;
end

if c2_charTime<1000
timeDependent_c2=c2*(1+0.5*(((t_real)^2)/((t_real)^2+c2_charTime^2)));
else
timeDependent_c2=c2;
end

dy(1) = a1 * st - a2 * y(1) + a3 * y(2);
dy(2) = ((c1 * st)/(timeDependent_c2 + st + c3 * y(1))) - ((y(2)^hillK)/(hillS^hillK+y(2)^hillK))* timeDependent_c5 - c4 * y(2);
